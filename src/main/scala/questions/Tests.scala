package questions

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by mark on 18/05/2017.
 */
object Tests extends App{
  val conf = new SparkConf().setAppName("Basic")
    .setMaster("local[*]")
  val sc = new SparkContext(conf)


  val ratings:RDD[(Int,Double)]={
    //val ratings = sc.textFile("dataset/small-ratings.csv")
    val ratingMaps =   sc.textFile("dataset/small-ratings.csv")
      .map(_.split(","))
      .filter(_(0)!="userId")
      //transform to map ,ratingRecord(movieId,rating,.....) to map(movieId,rating)
      .map(strs=>{
        (
          strs(1).toInt,    // movieId
          strs(2).toDouble  // rating
          )
      })

    //ratingMaps.foreach(println)
    // rating value transform key-value , (rating,count) = (rating,1)
    val ratingValues = ratingMaps.mapValues(rating => rating -> 1)
    val ratingSum = ratingValues.reduceByKey((acc,curr)=> {
      (acc._1+curr._1)->(acc._2+curr._2)
    })
    val ratingMeans = ratingSum.mapValues(v => (v._1 / v._2).toDouble)

    ratingMeans

  }

  //ratings.foreach(println)



}
